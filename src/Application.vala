/*
* Copyright (c) 2018 Max Kunzelmann
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public
* License along with this program; if not, write to the
* Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
* Boston, MA 02110-1301 USA
*
* Authored by: Max Kunzelmann <maxdev@posteo.de>
*/

public class HelloPackaging : Gtk.Application { 
    public HelloPackaging() {
        Object(
            application_id: "com.gitlab.maaxxs.hello-packaging",
            flags: ApplicationFlags.FLAGS_NONE
        );
    }

    protected override void activate() {
        // Create the base window
        var window = new Gtk.ApplicationWindow(this);
        window.default_height = 400;
        window.default_width = 600;
        window.title = _("Hello Packaging World");

        /***** Layouting *****/
        // Create a Gtk.Grid
        var grid = new Gtk.Grid();
        grid.orientation = Gtk.Orientation.VERTICAL;
        grid.row_spacing = 6;
        grid.column_spacing = 6;

        var hello_button = new Gtk.Button.with_label(_("Say Hello"));
        var hello_label = new Gtk.Label(null);

        var rotate_button = new Gtk.Button.with_label(_("Rotate"));
        var rotate_label = new Gtk.Label(_("Horizontal"));

        var show_button = new Gtk.Button.with_label(_("Show"));
        var title_label = new Gtk.Label(_("Notifications"));

        var replace_button = new Gtk.Button.with_label(_("Replace Notification"));


        // First row in grid
        grid.attach(hello_button, 0, 0, 1, 1);
        grid.attach_next_to(hello_label, hello_button, Gtk.PositionType.RIGHT, 1, 1);

        // Second row
        grid.attach(rotate_button, 0, 1, 1, 1);
        grid.attach_next_to(rotate_label, rotate_button, Gtk.PositionType.RIGHT, 1, 1);

        // third row
        grid.attach(show_button, 0, 2, 1, 1);
        grid.attach_next_to(title_label, show_button, Gtk.PositionType.RIGHT, 1, 1);

        // fourth row
        grid.attach(replace_button, 0, 3, 1, 1);


        // Add grid to Main Window
        window.add(grid);

        /***** Functions, Callbacks *****/
        hello_button.clicked.connect(() => {
            hello_label.label = _("HEllo my friend!");
        });

        rotate_button.clicked.connect(() => {
            if (rotate_label.angle == 0) {
                rotate_label.angle = 90;
                rotate_label.label = _("Vertical");
            } else {
                rotate_label.angle = 0;
                rotate_label.label = _("Horizontal");
            }
        });

        show_button.clicked.connect(() => {
            var notification = new Notification(_("Hello World"));
            var icon = new GLib.ThemedIcon("dialog-information");
            notification.set_icon(icon);
            notification.set_body(_("This is a great notification!"));
            this.send_notification("com.gitlab.maaxxs.hello-packaging", notification);
        });

        replace_button.clicked.connect(() => {
            var notification = new Notification(_("Replacement"));
            notification.set_body(_("This is the replacement notification"));

            var icon = new GLib.ThemedIcon("dialog-warning");
            notification.set_icon(icon);
            notification.set_priority(NotificationPriority.URGENT);
            this.send_notification("com.gitlab.maaxxs.hello-packaging", notification);
        });

        window.show_all();

    }

    public static int main(string[] args) {
        var app = new HelloPackaging();
        return app.run(args);
    }
}